@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                {!! Breadcrumbs::render() !!}
                <div class="panel panel-default">
                    <div class="panel-heading">EDIT PERMISSIONS</div>

                    <div class="panel-body">
                        <div class="row">
                            {!! Form::open() !!}
                            <div class="col-xs-12">
                                @if(Session::has('message'))
                                <div class="alert alert-{{ Session::get('alert-class', 'info') }}">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ Session::get('message') }}
                                </div>
                                @endif
                                <table class="table table-striped">
                                    <thead>
                                        <th style="width: 25%;">Permissions</th>
                                        @foreach ($roles as $role)
                                        <th style="text-align: center; width: {!!75/$roles->count()!!}%;">{!! $role->name !!}</th>
                                        @endforeach
                                    </thead>
                                    <tbody>
                                        @foreach ($permissions as $permission)
                                        <tr>
                                            <td style="font-size: 1.1em">{!! $permission->display_name !!}</td>
                                            @foreach ($roles as $index => $role)
                                            <td style="text-align: center;">
                                                {!! Form::checkbox("permissions[$role->name][{$permission->id}]", $permission->id, $role->can($permission)) !!}
                                            </td>
                                            @endforeach
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-xs-12">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-success">Save</button>
                                    <a href="{{ route('admin.roles.index') }}" class="btn btn-primary">Back</a>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
