@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			{!! Breadcrumbs::render() !!}
			<div class="panel panel-default">
				<div class="panel-heading">EDIT ROLE</div>

				<div class="panel-body">

                    @include('errors.list')

                    {!! Form::model($role, [ 'method' => 'PATCH', 'route' => [ 'admin.roles.update', $role["id"] ] ]) !!}
						@include('roles.form')

					    <div class="col-xs-12 text-right">
					        {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
					    	<a href="{{ route('admin.roles.show', [$role['id']]) }}" class="btn btn-primary">Cancel</a>
					    </div>

                    {!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
