@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">DASHBOARD</div>

                <div class="panel-body fb-dashboard">
                    <a href="{{ route('admin.items.index') }}">
                        <img src="{{ asset('/admin-assets/img/icon-prod.png')}}" alt=""/>
                        Item Management
                    </a>

                    <a href="{{ route('admin.products.index') }}">
                        <img src="{{ asset('/admin-assets/img/icon-gallery.png')}}" alt=""/>
                        Product Management
                    </a>

                    <a href="{{ route('admin.themes.index') }}">
                        <img src="{{ asset('/admin-assets/img/icon-theme.png')}}" alt=""/>
                        Theme Management
                    </a>


                    <a href="{{ route('admin.users.index') }}">
                        <img src="{{ asset('/admin-assets/img/icon-user.png')}}" alt=""/>
                        User Management
                    </a>

                    <a href="{{ route('admin.roles.index') }}">
                        <img src="{{ asset('/admin-assets/img/icon-role.png')}}" alt=""/>
                        Role Access Management
                    </a>
                </div>
			</div>
		</div>
	</div>
</div>
@endsection
