@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                {!! Breadcrumbs::render() !!}
                <div class="panel panel-default">
                    <div class="panel-heading">VIEW ITEM</div>

                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <h2>{{ $item['type_name'] }}</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h4>Texture:</h4>
                                <p class="text-center">
                                    <img src="{{$item['texture_path']}}" style="max-width: 100%;">
                                </p>
                            </div>
                            <div class="col-md-6">
                                <h4>Item ID:</h4>
                                <p>{{ $item['item_id'] }}</p>
                                <h4>Price:</h4>
                                <p>{{ $item['price'] }}</p>
                                <h4>Item properties:</h4>
                                <dl class="dl-horizontal">
                                    @foreach ($item['properties'] as $property)
                                        <dt>{{ $property['key_name'] }}</dt>
                                        <dd>{{ $property['value'] }}</dd>
                                    @endforeach
                                </dl>
                                <h4>Themes:</h4>
                                <ul>
                                    @foreach ($item['themes'] as $theme)
                                    <li>
                                        <a href="{{ route('admin.themes.show', [$theme['id']]) }}">
                                            {{ $theme['name'] }}
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 text-right">
                                <a href="{{ route('admin.items.edit', [$item['id']]) }}" class="btn btn-primary">Edit</a>
                                <a href="{{ route('admin.items.index') }}" class="btn btn-primary">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
