<link href="{{ asset('/admin-assets/css/bootstrap-colorpicker.min.css') }}"  rel="stylesheet">
<script src="{{ asset('/admin-assets/js/bootstrap-colorpicker.min.js') }}"></script>
<div class="row">
    <fieldset>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="item_id">Item ID</label>
      <div class="col-md-4">
      {!! Form::text('item_id', null, ['class'=>'form-control input-md', 'required']) !!}
      </div>
    </div>

    <!-- Select Basic -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="type">Item Type</label>
      <div class="col-md-4">
        {!! Form::select('type', [''=>'Please Select']+$itemTypesList, null, ['class'=>'form-control', 'required', 'id'=>'type']) !!}
      </div>
    </div>

    <script type="text/javascript">
    $(function() {
      var $materials = $('#materials'),
          $soles = $('#soles'),
          $print = $('#print'),
          $type = $('#type');

      var updateProperties = function() {

        switch($type.val()) {
          case '1': // Strap
            $materials.find('.radio:not(.strap)').hide();
            $materials.find('.strap').show().find('input').prop('disabled', false);
            $soles.slideUp();
            $print.slideUp();
            $materials.slideDown();
            break;
          case '2': // Sole
            $materials.find('.radio:not(.sole)').hide();
            $materials.find('.sole').show().find('input').prop('disabled', false);
            $materials.slideDown();
            $print.slideUp();
            $soles.slideDown();
            $soles.find('input').prop('disabled', false);
            break;
          case '3': // Print
            $materials.slideUp();
            $soles.slideUp();
            $print.slideDown();
            $print.find('input').prop('disabled', false);
            break;
          default:
            $materials.slideUp();
            $soles.slideUp();
            $print.slideUp();
        } // switch

      } // update

        $('#type').change(function(e) {
          resetInputs();
          updateProperties();
        });

      var resetInputs = function() {
        $('#materials, #soles, #print').find('input').prop('disabled', true);
      }


      // Initialize
      resetInputs();
      updateProperties();

    });

    </script>

    <!-- Textarea -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="description">Description</label>
      <div class="col-md-4">
      {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
      </div>
    </div>

    <!-- File Button -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="texture">Texture</label>
      <div class="col-md-4">
        @if (isset($item['texture_path']))
          <img src="{{$item['texture_path']}}" style="max-width: 100%; margin-bottom: 1em;" />
        @endif
        <input id="texture" name="texture" class="input-file" type="file" />
      </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="background_color">Background Color</label>
      <div class="col-md-4">
        <div class="input-group bgcolor">
        {!! Form::text('background_color', null, ['class'=>'form-control input-md']) !!}
          <span class="input-group-addon"><i style="border: 1px solid #cccccc"></i></span>
        </div>
      </div>
      <script>
          $(function(){
              $('.bgcolor').colorpicker();
          });
      </script>
    </div>

    <!-- Textarea -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="price">Price</label>
      <div class="col-md-4">
      {!! Form::text('price', null, ['class'=>'form-control']) !!}
      </div>
    </div>

    <!-- Multiple Radios (inline) -->
    <div class="form-group" id="print" style="display: none;">
      <label class="col-md-4 control-label" for="properties[vacuum_print]">Vacuum Print</label>
      <div class="col-md-4">
        <label class="checkbox-inline">
          {!! Form::hidden('properties[vacuum_print]', 0, null, ['disabled']) !!}
          {!! Form::checkbox('properties[vacuum_print]', 1, null, ['disabled']) !!}
          Yes
        </label>
      </div>
    </div>

    <!-- Multiple Radios -->
    <div class="form-group" id="materials" style="display: none;">
      <label class="col-md-4 control-label" for="properties[material_type]">Type of Material</label>
      <div class="col-md-4">
      <div class="radio strap">
        <label>
          {!! Form::radio('properties[material_type]', 'Rubber') !!}
          Rubber
        </label>
        </div>
      <div class="radio sole">
        <label>
          {!! Form::radio('properties[material_type]', 'EVA') !!}
          EVA
        </label>
        </div>
      <div class="radio strap sole">
        <label>
          {!! Form::radio('properties[material_type]', 'PVC') !!}
          PVC
        </label>
        </div>
      </div>
    </div>

    <!-- Multiple Radios (inline) -->
    <div class="form-group" id="soles" style="display: none;">
      <label class="col-md-4 control-label" for="properties[sole]">Sole</label>
      <div class="col-md-4">
        <label class="radio-inline">
          {!! Form::radio('properties[sole]', 'Insole', null, ['disabled']) !!}
          Insole
        </label>
        <label class="radio-inline">
          {!! Form::radio('properties[sole]', 'Outsole', null, ['disabled']) !!}
          Outsole
        </label>
      </div>
    </div>

    <!-- Select Multiple -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="themes">Themes</label>
      <div class="col-md-4">
        {!! Form::select('themes', $themesList, $itemThemes, ['class'=>'form-control', 'id'=>'themes', 'name'=>'themes[]', 'multiple']) !!}
      </div>
    </div>



    </fieldset>

</div>
