@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			{!! Breadcrumbs::render() !!}
			<div class="panel panel-default">
				<div class="panel-heading">ADD ITEM</div>

				<div class="panel-body">

                    @include('errors.list')

                    {!! Form::open([ 'route' => 'admin.items.store', 'files' => 'true', 'class' => 'form-horizontal']) !!}
						@include('items.form')
					    <div class="col-md-4 col-md-offset-4">
					        {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
					    	<a href="{{ route('admin.items.index') }}" class="btn btn-primary">Cancel</a>
					    </div>
                    {!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
