@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                {!! Breadcrumbs::render() !!}
                <div class="panel panel-default">
                    <div class="panel-heading">EDIT THEME</div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>{{ $theme['name'] }} <small>: {{ $theme['theme_id'] }}</small></h2>
                                <p>{{ $theme['description'] }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <dl class="dl">
                                    <dt>Background Image</dt>
                                    <dd class="text-center">
                                        <img src="{{ asset($theme['background_path']) }}" style="max-width: 100%;">
                                    </dd>
                                </dl>

                            </div>
                            <div class="col-md-6">
                                <dl class="dl">
                                    <dt>Description</dt>
                                    <dd><p>{{ $theme['description'] }}</p></dd>
                                    <dt>Gender</dt>
                                    <dd><p>{{ $theme['gender'] }}</p></dd>
                                    <dt>Age Demographic</dt>
                                    <dd><p>{{ $theme['age_demographic_name'] }}</p></dd>
                                    <dt>Age Range</dt>
                                    <dd><p>{{ $theme['age_range_name'] }}</p></dd>
                                </dl>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 text-right">
                                <a href="{{ route('admin.themes.edit', [$theme['id']]) }}" class="btn btn-primary">Edit</a>
                                <a href="{{ route('admin.themes.index') }}" class="btn btn-primary">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
