<div class="row">
    <fieldset>

    <!-- Select Basic -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="type">Size</label>
      <div class="col-md-4">
        {{-- {!! Form::select('size', [''=>'Please Select']+$sizeList, null, ['class'=>'form-control', 'required']) !!} --}}
        {!! Form::input('number', 'properties[size]', null, ['class'=>'form-control', 'min'=>6, 'max'=>'12', 'step'=>0.5]) !!}
      </div>
    </div>

    <!-- Multiple Radios -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="properties[size_cat]">Size Category</label>
      <div class="col-md-4">
      <div class="radio strap">
        <label>
          {!! Form::radio('properties[size_cat]', 'Male') !!} Male
        </label>
        </div>
      <div class="radio sole">
        <label>
          {!! Form::radio('properties[size_cat]', 'Female') !!} Female
        </label>
        </div>
      <div class="radio strap sole">
        <label>
          {!! Form::radio('properties[size_cat]', 'Kids') !!} Kids
        </label>
        </div>
      </div>
    </div>

    <!-- Select Basic -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="type">Strap</label>
      <div class="col-md-4">
        {!! Form::select('items[strap]', [''=>'Please Select']+$items['straps'], null, ['class'=>'form-control', 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="type">Strap Color</label>
      <div class="col-md-4">
        {!! Form::select('properties[strap_color]', [''=>'Please Select']+$colors, null, ['class'=>'form-control', 'required']) !!}
      </div>
    </div>

    <!-- Select Basic -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="type">Print Design</label>
      <div class="col-md-4">
        {!! Form::select('items[print_design]', [''=>'Please Select']+$items['print_design'], null, ['class'=>'form-control', 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="type">Sole Color</label>
      <div class="col-md-4">
        {!! Form::select('properties[sole_color]', [''=>'Please Select']+$colors, null, ['class'=>'form-control', 'required']) !!}
      </div>
    </div>

{{--     <!-- Select Multiple -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="themes">Theme</label>
      <div class="col-md-4">
        {!! Form::select('themes', $themes, null, ['class'=>'form-control', 'name'=>'themes']) !!}
      </div>
    </div>
 --}}


    </fieldset>

</div>
