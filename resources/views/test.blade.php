<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Footbook</title><link href='//fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="style.css">
    <script src="js/vendor.js"></script>
    <script src="js/app.js"></script>
  </head>
  <body class="hide">
    <div class="container">
    <div class="page-start">
        <h1><img src="images/logo.svg"></h1>
        <h2>Create your own flip-flops.</h2>
        <p>Tap anywhere to start</p><a href="#">&nbsp;</a>
      </div>
      <div class="page-choose-theme">
        <div class="header">
          <div class="logo"><img src="images/logo.svg">
            <h2>Custom Shop</h2>
          </div>
          <div class="nav"><a class="btn btn-home">Home</a><a class="btn btn-cart">Cart</a>
            <!--.btn.btn-username Hi, Username! <a href="#" class="btn btn-logout">LOGOUT</a>--><a class="btn btn-login">Login</a>
          </div>
        </div>
        <h1>Pick a them you want to start from.</h1>
        <ul class="theme-choices">
        @foreach ($themes as $theme)
          <li>
            <div>
              <div @if(!empty($theme->background_path)) style="background-image: url('{{ asset($theme->background_path) }}')" @endif class="image"></div><a data-theme="{{ str_slug($theme->theme_id) }}" class="btn btn-white">{{ $theme->name }}</a> </div>
          </li>
        @endforeach
{{--           <li>
            <div>
              <div style="background-image: url('images/themes/1.jpg')" class="image"></div><a data-theme="theme-summer" class="btn btn-white">City Theme</a>
            </div>
          </li>
          <li>
            <div>
              <div style="background-image: url('images/themes/2.jpg')" class="image"></div><a data-theme="theme-baseball" class="btn btn-white">City Theme</a>
            </div>
          </li>
          <li>
            <div>
              <div style="background-image: url('images/themes/3.jpg')" class="image"></div><a data-theme="theme-blue" class="btn btn-white">City Theme</a>
            </div>
          </li> --}}
        </ul><a class="btn btn-white bx-next"><span class="glyphicon glyphicon-chevron-right"></span></a><a class="btn btn-white bx-prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      </div>
      <div class="page-home">
        <h1><img src="images/logo.svg"></h1>
        <div class="home-gallery">
          <p>See masterpieces that have been created by others</p><a href="#" class="btn btn-white">Design Gallery</a><br>
          <a href="#" class="btn btn-white">indust sample</a>

        </div>
        <div class="home-create"><img src="images/home-slipper2.svg"><a href="#" class="btn btn-green btn-create-your-own">Create Your Own</a></div>
        <div class="home-signup"><img src="images/home-slipper3.svg"><a href="#" class="btn btn-white btn-signup">Signup / Login</a></div>
      </div>
      <div class="page-creator">
        <div class="header">
          <div class="logo"><img src="images/logo.svg">
            <h2>Custom Shop</h2>
          </div>
          <div class="nav"><a class="btn btn-home">Home</a><a class="btn btn-cart">Cart</a>
            <!--.btn.btn-username Hi, Username! <a href="#" class="btn btn-logout">LOGOUT</a>--><a class="btn btn-login">Login</a>
          </div>
        </div>
        <div class="body">
          <div class="options">
            <div class="wrap">
          <!--  <ul class="options-header">
                <li><a data-open=".options-size" class="active">Size</a></li>
                <li><a data-open=".options-style">Style</a></li>
                <li><a data-open=".options-design">Design</a></li>
              </ul> -->
              <div class="options-item options-size ops">
                <ul id="cat">
                  <li class="active"><a>Male</a></li>
                  <li><a>Female</a></li>
                  <li><a>Kids</a></li>
                </ul>
                <div class="newdesign">
                 <div class="clear"></div>
                <h3>Print Design</h3>

                <ul id="print_design" class="js-carousel options-design-print-design  newprint">
                  @foreach ($items['Print Design'] as $item)
                    <li><a data-item-id="{{ $item->id }}" data-image="{{ asset($item->texture_path) }}"></a></li>
                  @endforeach
{{--               <li class="active"><a data-item-id="1" data-image="assets/patterns/congruent_pentagon.png"></a></li>
                  <li><a data-item-id="1" data-image="assets/patterns/restaurant.png"></a></li>
                  <li><a data-item-id="2" data-image="assets/patterns/seamless_paper_texture.png"></a></li>
                  <li><a data-item-id="3" data-image="assets/patterns/upfeathers.png"></a></li>
                  <li><a data-item-id="4" data-image="assets/patterns/bright_squares.png"></a></li>
                  <li><a data-item-id="5" data-image="assets/patterns/green_gobbler.png"></a></li>
                  <li><a data-item-id="6" data-image="assets/patterns/skulls.png"></a></li>
                  <li><a data-item-id="7" data-image="assets/patterns/stardust.png"></a></li>
 --}}                </ul>
                
           
                  
          
            <div class="options-style">
                <h3>Strap Style</h3>
                <ul class="js-carousel options-style-strap sole" id="strap_style">
                  @foreach ($items['Strap'] as $item)
                    <li><a data-item-id="{{ $item->id }}"><img src="{{ asset($item->texture_path) }}"></a></li>
                  @endforeach
{{--              <li class="active"><a data-item-id="1"><img src="assets/straps/strap-00.svg"></a></li>
                  <li><a data-item-id="2"><img src="assets/straps/strap-01.svg"></a></li>
                  <li><a data-item-id="3"><img src="assets/straps/strap-02.svg"></a></li>
                  <li><a data-item-id="4"><img src="assets/straps/strap-03.svg"></a></li>
 --}}                </ul>
                <h3>Sole Style</h3>
                <ul class="js-carousel sole">
                  <li class="active"><a>Sole 1</a></li>
                  <li><a>Sole 2</a></li>
                  <li><a>Sole 3</a></li>
                </ul>
                <ul id="strap_color" style="top: 50px;" class="options-style-color active">
                  <li class="active"><a data-color="#FFFFFF">White</a></li>
                  <li><a data-color="#808285">Grey</a></li>
                  <li><a data-color="#000000">Black</a></li>
                  <li><a data-color="#0A4436">Green</a></li>
                  <li><a data-color="#FD6B35">Orange</a></li>
                  <li><a data-color="#F6B332">Yellow</a></li>
                  <li><a data-color="#D0112B">Red</a></li>
                </ul>
                <ul id="sole_color" style="top: 190px;" class="options-style-color">
                  <li class="active"><a data-color="#FFFFFF">White</a></li>
                  <li><a data-color="#808285">Grey</a></li>
                  <li><a data-color="#000000">Black</a></li>
                  <li><a data-color="#0A4436">Green</a></li>
                  <li><a data-color="#FD6B35">Orange</a></li>
                  <li><a data-color="#F6B332">Yellow</a></li>
                  <li><a data-color="#D0112B">Red</a></li>
                </ul>
                <div class="clear"></div>
              </div>





               <h3>Strap Accessories</h3>
                <ul class="js-carousel options-strap-accessories newprint">
                  @foreach ($items['Accessory'] as $item)
                    <li><a data-price="{{ number_format($item->price, 2) }}"><img src="{{ asset($item->texture_path) }}"></a></li>
                  @endforeach
{{--                   <li><a data-price="50.00"><img src="assets/accessory/1.png"></a></li>
                  <li><a data-price="30.00"><img src="assets/accessory/2.png"></a></li>
                  <li><a data-price="20.00"><img src="assets/accessory/3.png"></a></li>
 --}}                </ul>







                    </div>
                 <!--  <li class="active"><a>6</a></li>
                  <li><a>6.5</a></li>
                  <li><a>7</a></li>
                  <li><a>7.5</a></li>
                  <li><a>8</a></li>
                  <li><a>8.5</a></li>
                  <li><a>9</a></li>
                  <li><a>9.5</a></li>
                  <li><a>10</a></li>
                  <li><a>10.5</a></li>
                  <li><a>11</a></li>
                  <li><a>11.5</a></li>
                  <li><a>12</a></li> -->
  
              </div>

              <div class="options-item options-style">
                <h3>Strap Style</h3>
                <ul class="js-carousel options-style-strap" id="strap_style">
                  @foreach ($items['Strap'] as $item)
                    <li><a data-item-id="{{ $item->id }}"><img src="{{ asset($item->texture_path) }}"></a></li>
                  @endforeach
{{--              <li class="active"><a data-item-id="1"><img src="assets/straps/strap-00.svg"></a></li>
                  <li><a data-item-id="2"><img src="assets/straps/strap-01.svg"></a></li>
                  <li><a data-item-id="3"><img src="assets/straps/strap-02.svg"></a></li>
              
 --}}                </ul>
                <h3>Sole Style</h3>
                <ul class="js-carousel">
                  <li class="active"><a>Sole 1</a></li>
                  <li><a>Sole 2</a></li>
                  <li><a>Sole 3</a></li>
                </ul>
                <ul id="strap_color" style="top: 50px;" class="options-style-color active">
                  <li class="active"><a data-color="#FFFFFF">White</a></li>
                  <li><a data-color="#808285">Grey</a></li>
                  <li><a data-color="#000000">Black</a></li>
                  <li><a data-color="#0A4436">Green</a></li>
                  <li><a data-color="#FD6B35">Orange</a></li>
                  <li><a data-color="#F6B332">Yellow</a></li>
                  <li><a data-color="#D0112B">Red</a></li>
                </ul>
                <ul id="sole_color" style="top: 190px;" class="options-style-color">
                  <li class="active"><a data-color="#FFFFFF">White</a></li>
                  <li><a data-color="#808285">Grey</a></li>
                  <li><a data-color="#000000">Black</a></li>
                  <li><a data-color="#0A4436">Green</a></li>
                  <li><a data-color="#FD6B35">Orange</a></li>
                  <li><a data-color="#F6B332">Yellow</a></li>
                  <li><a data-color="#D0112B">Red</a></li>
                </ul>
              </div>
              <div class="options-item options-design">
                <!--h3 Strap Texture-->
                <!--ul.js-carousel
                li.active: a Texture 1
                li: a Texture 2
                li: a Texture 3

                -->
                <h3>Print Design</h3>
                <ul id="print_design" class="js-carousel options-design-print-design">
                  @foreach ($items['Print Design'] as $item)
                    <li><a data-item-id="{{ $item->id }}" data-image="{{ asset($item->texture_path) }}"></a></li>
                  @endforeach
{{--                   <li class="active"><a data-item-id="1" data-image="assets/patterns/congruent_pentagon.png"></a></li>
                  <li><a data-item-id="1" data-image="assets/patterns/restaurant.png"></a></li>
                  <li><a data-item-id="2" data-image="assets/patterns/seamless_paper_texture.png"></a></li>
                  <li><a data-item-id="3" data-image="assets/patterns/upfeathers.png"></a></li>
                  <li><a data-item-id="4" data-image="assets/patterns/bright_squares.png"></a></li>
                  <li><a data-item-id="5" data-image="assets/patterns/green_gobbler.png"></a></li>
                  <li><a data-item-id="6" data-image="assets/patterns/skulls.png"></a></li>
                  <li><a data-item-id="7" data-image="assets/patterns/stardust.png"></a></li>
 --}}                </ul>
                <div class="options-design-upload"><a class="btn btn-orange btn-small btn-upload">Upload</a>
                  <input type="file" name="image" id="upload" onchange="previewImage(this)" accept="image/*"><a class="btn btn-orange btn-small btn-upload-move-up"><span class="glyphicon glyphicon-arrow-up"></span></a><a class="btn btn-orange btn-small btn-upload-move-down"><span class="glyphicon glyphicon-arrow-down"></span></a><a class="btn btn-orange btn-small btn-upload-move-left"><span class="glyphicon glyphicon-arrow-left"></span></a><a class="btn btn-orange btn-small btn-upload-move-right"><span class="glyphicon glyphicon-arrow-right"></span></a><br><a class="btn btn-orange btn-small btn-upload-zoom-in"><span class="glyphicon glyphicon-zoom-in"></span></a><a class="btn btn-orange btn-small btn-upload-zoom-out"><span class="glyphicon glyphicon-zoom-out"></span></a><a class="btn btn-orange btn-small btn-mirror">Mirror</a>
                  <!--a.btn.btn-orange.btn-small.btn-remove-upload Remove-->
                </div>
                <h3>Strap Accessories</h3>
                <ul class="js-carousel options-strap-accessories">
                  @foreach ($items['Accessory'] as $item)
                    <li><a data-price="{{ number_format($item->price, 2) }}"><img src="{{ asset($item->texture_path) }}"></a></li>
                  @endforeach
{{--                   <li><a data-price="50.00"><img src="assets/accessory/1.png"></a></li>
                  <li><a data-price="30.00"><img src="assets/accessory/2.png"></a></li>
                  <li><a data-price="20.00"><img src="assets/accessory/3.png"></a></li>
 --}}                </ul>
              </div>
            </div>
          </div>
          <div class="main">
            <div class="main-place-accessory cover">
              <h2>Drag your accessory into position</h2>
            </div>
            <div class="main-place-accessory">
              <div class="btn-container"><a class="btn btn-red btn-cancel">Cancel</a><a class="btn btn-green-light btn-done">Done</a></div>
            </div>
            <div class="main-tools"><a class="btn btn-orange btn-undo">Undo</a><a class="btn btn-orange btn-redo">Redo</a>
              <div class="main-tools-zoom"><a class="btn btn-orange btn-zoom-out">Zoom out</a>
                <p>100%</p><a class="btn btn-orange btn-zoom-in">Zoom in</a>
              </div><a class="btn btn-green-light btn-save">Save &amp; Preview</a>
            </div>
            <div class="main-show">
              <div class="main-show-top">
                <!-- Slippers svg container-->
              </div>
              <div class="main-show-left">
                <!-- Slippers svg container-->
                <p>&nbsp;</p>
              </div>
              <div class="main-show-right">
                <!-- Slippers svg container-->
                <p>&nbsp;</p>
              </div>
            </div>
            <div class="main-other">
              <div class="main-view">
                <div class="main-view-top active">
                  <!--include images/preview-top.svg-->
                </div>
                <div class="main-view-left">
                  <!--include images/preview-left.svg-->
                </div>
                <div class="main-view-right"></div>
                <!--.main-view-diagonal
                img(src="images/preview-diagonal.svg")

                -->
              </div>
              <div class="main-checkout">
                <p>PHP <b>250.00</b></p><a class="btn btn-green-light btn-checkout">Checkout</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="popup popup-get-ticket"><a class="btn-close">Close</a><img src="images/modal-signin.svg">
      <p>Please get your order ticket and present to the cashier</p><a class="btn btn-green-light btn-done">Done</a>
    </div>
    <div class="popup popup-checkout"><a class="btn-close">Close</a>
      <div class="popup-checkout-left">
        <div class="center"><img src="assets/slippers/slippers.svg" width="280"></div>
        <div class="center"><a class="btn btn-share">Share it!</a></div>
      </div>
      <div class="popup-checkout-right">
        <h2>Summary</h2>
        <table>
          <tr>
            <th>Item</th>
            <th>Price</th>
          </tr>
          <tr>
            <td>Flip-flops</td>
            <td><span>PHP</span> 150.00</td>
          </tr>
          <tr>
            <td>Print</td>
            <td><span>PHP</span> 100.00</td>
          </tr>
          <tr>
            <td>Accessory 1 (x1)</td>
            <td><span>PHP</span> 50.00</td>
          </tr>
          <tr>
            <td>Accessory 2 (x1)</td>
            <td><span>PHP</span> 50.00</td>
          </tr>
        </table>
        <table class="popup-checkout-subtotal">
          <tr>
            <td>Subtotal</td>
            <td><span>PHP</span> 350.00</td>
          </tr>
        </table>
        <table class="popup-checkout-grandtotal">
          <tr>
            <td>Quantity</td>
            <td>
              <input type="text" value="1">
            </td>
          </tr>
          <tr>
            <td>Grand Total</td>
            <td><span>PHP</span> 350.00</td>
          </tr>
        </table><a class="btn btn-green-light btn-checkout-final">Checkout</a>
      </div>
    </div>
    <div class="popup popup-signin">
      <div style="width: 25%; font-weight: 300; opacity: 0.8">
        <p>Hey there,</p>
        <p>You can login to see you previous works and unfinished designs</p>
      </div>
      <div style="width: 40%; padding-top: 2em;">
        <div style="width: 50%; float: left;">
          <input type="text" placeholder="Username" class="js-keyboard">
        </div>
        <div style="width: 50%; float: left;">
          <input type="password" placeholder="Password" class="js-keyboard">
        </div><a style="width: 94%" href="industrial.html" class="btn btn-small btn-green-light">Login</a>
      </div>
      <div style="width: 30%; padding-top: 2.70em;">
        <p style="margin: 0; padding: 0; font-weight: 300; opacity: 0.8; font-size: 0.9em;">Don't have an account?</p><a style="width: 100%" class="btn btn-small btn-blue">Register</a>
      </div><a style="cursor: pointer; position: absolute; top: 5px; right: 10px; text-transform: uppercase; opacity: 0.8" class="btn-skip">Skip »</a>
    </div>
    <div class="popup popup-accessory">
      <h2>Add this strap accessory?</h2>
      <div><img src="assets/accessory/1.png"></div>
      <p class="currency">+ <span>PHP</span> <strong>50.00</strong></p><a class="btn btn-red js-close-popup">No</a><a class="btn btn-green-light js-add-accessory">Yes</a>
    </div>
  </body>
</html>
<div class="pattern"></div>
<div class="accessory-close"></div>
