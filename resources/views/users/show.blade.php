@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                {!! Breadcrumbs::render() !!}
                <div class="panel panel-default">
                    <div class="panel-heading">VIEW USER</div>

                    <div class="panel-body">
                        <div class="row">
                            @if (empty($user->photo_path))
                            <div class="col-xs-12">
                            @else
                            <div class="col-xs-8">
                            @endif
                                <table class="table table-striped">
                                    <tr>
                                        <td>Name:</td>
                                        <td>{{ $user->full_name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Role:</td>
                                        <td>{{ $user->roles->first()->name or '' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Address:</td>
                                        <td>
                                            <p>{{ $user->address_1 }}</p>
                                            @if (isset($user->address_2))
                                                <p>{{ $user->address_2 }}</p>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Contact Number:</td>
                                        <td>{{ $user->contact_num }}</td>
                                    </tr>
                                    <tr>
                                        <td>Status:</td>
                                        <td>{{ $user->status_value }}</td>
                                    </tr>
                                </table>
                            </div>
                            @if (!empty($user->photo_path))
                            <div class="col-xs-4">
                                    <img src="{{ asset($user->photo_path) }}" style="max-width: 100%">
                            </div>
                            @endif
                            <div class="col-xs-12">
                                <a href="{{ route('admin.users.edit', $user) }}" class="btn btn-primary">Edit</a>
                                <a href="{{ route('admin.users.index') }}" class="btn btn-primary">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
