@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
            {!! Breadcrumbs::render() !!}
			<div class="panel panel-default">
				<div class="panel-heading">USER MANAGEMENT</div>

				<div class="panel-body">


                    <div class="row paging">
                        <div class="col-md-6">
                            {!! $users->render() !!}
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{ route('admin.users.create') }}" class="btn btn-primary">Add User</a>
                        </div>
                    </div>

                    <table class="table table-responsive">
                        <tr>
                            <th>ID</th>
                            <th>Email</th>
                            <th>Name</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th></th>
                        </tr>

                        @foreach ($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->full_name }}</td>
                            <td>{{ $user->roles->first()->name or '' }}</td>
                            <td>{{ $user->status_value }}</td>
                            <td class="text-right">
                                <a href="{{ route('admin.users.show', [$user]) }}" class="btn btn-default btn-xs"
                                   data-toggle="tooltip" data-placement="top" title="View">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                                <a href="{{ route('admin.users.edit', [$user]) }}" class="btn btn-default btn-xs"
                                   data-toggle="tooltip" data-placement="top" title="Edit">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                                <a href="{{ route('admin.users.destroy', [$user]) }}" class="btn btn-default btn-xs btn-danger delete-confirm"
                                   data-toggle="tooltip" data-placement="top" title="Delete" data-item="user">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                        @endforeach

                    </table>
                    <div class="row paging">
                        <div class="col-md-6">
                            {!! $users->render() !!}
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{ route('admin.users.create') }}" class="btn btn-primary">Add User</a>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
