<?php

use Illuminate\Database\Seeder;
use App\Theme;

class ThemesTableSeeder extends Seeder {

    public function run()
    {
    	$themes = [
    		[
    			'theme_id' => 'theme-baseball',
    			'name' => 'Sports',
    			'background_path' => 'images/themes/1.jpg'
    		],
    		[
    			'theme_id' => 'theme-blue',
    			'name' => 'City',
    			'background_path' => 'images/themes/2.jpg'
    		],
    		[
    			'theme_id' => 'theme-summer',
    			'name' => 'Summer',
    			'background_path' => 'images/themes/3.jpg'
    		]
    	];

    	foreach ($themes as $theme) {
    		Theme::create($theme);
    	}

    }

}
