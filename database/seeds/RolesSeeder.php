<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;

class RolesSeeder extends Seeder {

	public function run()
	{

		// Roles
		$admin = Role::firstOrCreate(['name'=>'Administator']);
		$staff = Role::firstOrCreate(['name'=>'Staff']);
		$industrial = Role::firstOrCreate(['name'=>'Industrial']);
		$consumer = Role::firstOrCreate(['name'=>'Consumer']);

		// Superadmin
		$superAdmin = App\User::firstOrCreate([
			'first_name' => 'Superadm1n',
			'email' => 'gat@outlook.ph',
			'password' => bcrypt('passw0rd')
		])->attachRoles([$admin, $industrial, $staff]);

		// Permissions
		$viewAdmin = Permission::firstOrCreate([
			'name' => 'view_admin',
			'display_name' => 'View admin'
		]);
		$manageUsers = Permission::firstOrCreate([
			'name' => 'manage_users',
			'display_name' => 'Manage users'
		]);
		$bulkOrder = Permission::firstOrCreate([
			'name' => 'bulk_order',
			'display_name' => 'Bulk order'
		]);
		$manageItems = Permission::firstOrCreate([
			'name' => 'manage_items',
			'display_name' => 'Manage items'
		]);
		$approveDesigns = Permission::firstOrCreate([
			'name' => 'approve_designs',
			'display_name' => 'Approve designs'
		]);

		// Sync permissions
		$admin->perms()->sync([
			$viewAdmin->id,
			$manageUsers->id,
			$bulkOrder->id,
			$manageItems->id,
			$approveDesigns->id
		]);
		$industrial->perms()->sync([
			$bulkOrder->id
		]);
		$staff->perms()->sync([
			$viewAdmin->id,
			$manageItems->id
		]);

	}

}
