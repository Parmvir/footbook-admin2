<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductProperty extends Model {

	protected $fillable = ['product_id', 'key', 'value'];


	public function product()
	{
		return $this->belongsTo('App\Product');
	}

}
