<?php namespace App\Http\Traits;

use Cache;
use Carbon\Carbon;
use App\Lookups;

trait LookupsValue {

	/**
	 * Get values from lookups table
	 * and store it in cache to fix N+1 problem
	 *
	 * @return string
	 * @author Gat
	 **/
	public static function getLookupValue($key, $id = null)
	{

		if (Cache::has("{$key}_{$id}")) {
			return Cache::get("{$key}_{$id}");
		}

		$lookupValue = Lookups::whereKey($key);

		if (isset($id))
			$lookupValue->whereKeyId($id);

		// Set expiry for 1 day
		$duration = Carbon::now()->addDay();

		$value = $lookupValue->first()->value;

		// Store in cache
		Cache::put("{$key}_{$id}", $value, $duration);

		return $value;
	}

}
