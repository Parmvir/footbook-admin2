<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateThemeRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return false;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'theme_id' => 'required|unique:themes,theme_id,'.$this->themes,
			'name' => 'string|required',
			'background' => 'mimes:jpeg,png'
		];
	}

}
