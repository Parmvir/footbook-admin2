<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreItemRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'item_id' => 'required|unique:items',
			'description' => 'string',
			'type' => 'required|integer',
			'texture' => 'required|mimes:jpeg,png',
			'background_color' => ['regex:/^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/'],
			'price' => 'numeric'
		];
	}

}
