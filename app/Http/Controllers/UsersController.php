<?php namespace App\Http\Controllers;

use App\User;
use App\UserInfo;
use App\Role;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Contracts\Auth\Guard;

use Illuminate\Http\Request;

class UsersController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// Hide superadmin
		$users = User::with('roles')->where('id', '!=', 1)->paginate(10);

		return view('users.index', compact('users'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$user = null;
		$roles = Role::all()->lists('name', 'id');
		return view('users.create', compact('user', 'roles'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Requests\StoreUserRequest $request, Registrar $registrar, Guard $auth)
	{
		$input = $request->all();

		$validator = $registrar->validator($input);

		if ($validator->fails())
		{
			$this->throwValidationException(
				$request, $validator
			);
		}

		$user = $registrar->create($input);

		if ($photo = array_pull($input, 'photo')) {
			$folder = '/uploads/users/photos/';
			$filename = $user->id . '.' . $photo->getClientOriginalExtension();
			$path = $folder . $filename;
			$photo->move(public_path($folder), $filename);

			$user->photo_path = $path;
			$user->save(); // Update user with photo
		}


		$user->roles()->sync($request->only('role'));

		return redirect()->route('admin.users.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::with('roles')->find($id);

		return view('users.show', compact('user'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// Block editing of superadmin
		if ($id == 1) {
			abort(403, 'Nice try.');
		}

		$user = User::find($id)->load(['roles']);
		$roles = Role::all()->lists('name', 'id');
		return view('users.edit', compact('user', 'roles'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Requests\UpdateUserRequest $request)
	{
		$user = User::with('roles')->find($id);

		$input = $request->all();

		if (empty($input['password']) && empty($input['password_confirmation'])) {
			array_pull($input, 'password');
		} else {
			$input['password'] = bcrypt($input['password']);
		}

		$user->update($input);
		$user->roles()->sync($request->only('role'));

		if ($photo = array_pull($input, 'photo')) {
			$folder = '/uploads/users/photos/';
			$filename = $id . '.' . $photo->getClientOriginalExtension();
			$path = $folder . $filename;
			$photo->move(public_path($folder), $filename);

			$user->photo_path = $path;
			$user->save(); // Update user with photo
		}

		return redirect()->route('admin.users.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		User::find($id)->delete();

		return redirect()->route('admin.users.index');
	}

}
