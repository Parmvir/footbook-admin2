<?php namespace App\Http\Controllers;

use App\Item;
use App\Theme;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$items = Item::all()->groupBy('type_name');
		$themes = Theme::all();

		return view('footbook', compact('items', 'themes'));
	}

	public function admin()
	{
		$this->middleware('auth');
		return view('home');
	}

	public function industrial()
	{
		$items = Item::all()->groupBy('type_name');
		$themes = Theme::all();

		return view('test', compact('items', 'themes'));
	}

}
