<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\LookupsValue as LookupsValue;
use Nicolaslopezj\Searchable\SearchableTrait;

class Item extends Model {

	use LookupsValue;
	use SearchableTrait;

	protected $fillable = [
		'item_id',
		'description',
		'type',
		'texture_path',
		'background_color',
		'price'
	];

	protected $appends = ['type_name'];

	protected $searchable = [
		'columns' => [
			'item_id' => 10,
			'description' => 5,
			'price' => 5
		]
	];

	public function properties()
	{
		return $this->hasMany('App\ItemProperty');
	}

	public function getTypeNameAttribute()
	{
		if (isset($this->attributes['type']))
			return $this->getLookupValue('item_type', $this->attributes['type']);
	}

	public function themes()
	{
		return $this->belongsToMany('App\Theme');
	}

	public function products()
	{
		return $this->belongsToMany('App\Product');
	}

}
