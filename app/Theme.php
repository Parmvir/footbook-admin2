<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\LookupsValue as LookupsValue;
use Nicolaslopezj\Searchable\SearchableTrait;

class Theme extends Model {

	use LookupsValue;
	use SearchableTrait;

	protected $fillable = [
		'theme_id',
		'name',
		'description',
		'gender',
		'age_demographic',
		'age_range',
		'background_path'
	];

	protected $appends = ['age_demographic_name', 'age_range_name'];

	protected $searchable = [
		'columns' => [
			'theme_id' => 10,
			'name' => 10,
			'description' => 5
		]
	];


	public function getAgeDemographicNameAttribute()
	{
		if (isset($this->attributes['age_demographic']))
			return $this->getLookupValue('age_demographic', $this->attributes['age_demographic']);
	}

	public function getAgeRangeNameAttribute()
	{
		if (isset($this->attributes['age_range']))
			return $this->getLookupValue('age_range', $this->attributes['age_range']);
	}

	public function items()
	{
		return $this->hasMany('App\Item');
	}

}
